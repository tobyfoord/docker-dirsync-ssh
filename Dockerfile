FROM alpine

MAINTAINER Toby Foord <toby@idodev.co.uk>

RUN apk add --update --no-cache rsync openssh sshpass

# Make dirs
RUN mkdir /sync && mkdir /keys

# Expose them as volumes
VOLUME ["/sync","/keys"]

# Value to hold commection for remote in rsync - to delete value
ENV RSYNC_REMOTE "user@host:path"

# Copy in the script
ADD script.sh /tmp/script.sh

RUN chmod +x /tmp/script.sh

# run the script
ENTRYPOINT ["/tmp/script.sh"]



