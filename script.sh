#!/bin/sh

# move keys to default location
mkdir ~/.ssh
cp /keys/* ~/.ssh

#fix permissions (ssh will moan otherwise)
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub

ssh-keyscan $SYNC_HOST >> ~/.ssh/known_hosts

ssh "${SYNC_USER}@${SYNC_HOST}" "cd ${SYNC_PATH} && find . -type f -mtime -${SYNC_DAYS}" > /sync/filelist

rsync -vra --files-from=/sync/filelist --no-dirs "${SYNC_USER}@${SYNC_HOST}:${SYNC_PATH}" /sync
