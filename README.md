Docker Directory Sync over SSH

This tool allows the automated syncronisation of a local folder with a remote ssh endpoint.

The docker run command must look a little like the following:

```
docker run -v \local\path\to\sync\:/sync -v \local\ssh\key\files\:/keys -e "SYNC_USER=username" -e "SYNC_HOST=domain.com" -e "SYNC_PATH=/remote/path" idodev/rsync-ssh-remote
```

The docker image takes two volumes

# Volumes

**/sync**

This is the local directory that will recieve the remote files

**/keys**

To use this image you need to provide ssh keys that have been authorised against the remote host.

This is expected to be both an `id_rsa` and `id_rsa.pub` file.

# Environment Variables

+ **SYNC_USER** - ssh user name
+ **SYNC_HOST** - remote host
+ **SYNC_PATH** - the path on the remote host to syncronise.